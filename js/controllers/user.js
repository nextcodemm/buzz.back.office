angular
.module('app')
.controller('UserController',
    ['$scope', '$stateParams', '$http', '$location','urls', 
    function($scope, $stateParams, $http, $location, urls) {
        var self = this;
        self.user = {};
        self.users = [];

        if($stateParams.user != null){
            //console.log($stateParams.user);
            self.user = $stateParams.user;
        }

        $http.get(urls.USER_SERVICE_API + "users/getallusers")
            .then(
                function (response) {
                    console.log('Fetched successfully all users');
                    self.users = response.data.users;
                },
                function (errResponse) {
		            console.error('Error while loading users');
		        }
            );
    }
]);