angular
.module('app')
.controller('DoctorController',
    ['$scope', '$stateParams', '$http', '$location','urls', 
    function($scope, $stateParams, $http, $location, urls) {
        var self = this;
        self.doctor = {};
        self.doctors = [];

        self.totalPages = 0;
        self.pageNumber = 1;
        self.moveTo = moveTo;
        self.movePrev = movePrev;
        self.moveNext = moveNext;
        self.approveDoctor = approveDoctor;

        if($stateParams.doctor != null){
            console.log($stateParams.doctor);
            self.doctor = $stateParams.doctor;
        }

        $http.get(urls.USER_SERVICE_API + "doctors/totalcount")
        .then(
            function (response) {
                self.totalPages = parseInt(response.data.count / 10);
                console.log(self.totalPages);

                $http.get(urls.USER_SERVICE_API + "doctors/getAllDoctors/1/10")
                .then(
                    function (response) {
                        console.log('Fetched successfully all doctors');
                        self.doctors = response.data.doctors;
                        self.pageNumber = 1;
                        //console.log(self.doctors);
                    },
                    function (errResponse) {
                        console.error('Error while loading doctors');
                    }
                );
            },
            function (errResponse) {
                console.error('Error while loading page count');
            }
        );

        function approveDoctor(doctorid) {
            console.log("approved!" + doctorid);
            $http.post(urls.USER_SERVICE_API + "doctors/approveDoctor/" + doctorid)
            .then(
                function (response) {
                    console.log('Approve successfully');
                    $location.path("/doctors");
                },
                function (errResponse) {
                    console.error('Error while approving doctor');
                }
            );
        }

        function moveTo(n) {
            n--;
            var url = urls.USER_SERVICE_API + "doctors/getAllDoctors/" + n + "/10";
            loadDoctors(url);
        }

        function movePrev(){
            if(self.pageNumber > 0) {
                var n = self.pageNumber - 1;
                var url = urls.USER_SERVICE_API + "doctors/getAllDoctors/" + n + "/10";
                loadUsers(url);
            }
        }

        function moveNext(){
            if(self.pageNumber < self.totalPages){
                var n = self.pageNumber + 1;
                var url = urls.USER_SERVICE_API + "doctors/getAllDoctors/" + n + "/10";
                loadDoctors(url);
            }
        }

        function loadDoctors(url) {
            $http.get(url)
            .then(
                function (response) {
                    self.doctors = response.data.doctors;
                },
                function (errResponse) {
                    console.error('Error while loading HealthEducation');
                }
            );
        }
    }
]);